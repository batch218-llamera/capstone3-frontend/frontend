import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link   } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function OrderView(details) {

const {_id} = details

    const { user } = useContext(UserContext);
    const navigate = useNavigate(); 
    const { userId } = useParams();
    const [firstName, setFirstName] = useState("");
    const [name, setName] = useState("");
    const [cart, setCart] = useState("");
    const [productId , setProductId] = useState("")
 
    return (

        <Container>
            <Row>
                <Col lg={{span: 6, offset:3}} >
                    <Card>
                          <Card.Body className="text-center">
                            <Card.Title>{firstName}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{cart}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {productId}</Card.Text>
                            <Button className="bg-primary" as={Link} to={`/user/${_id}`} >Product Details</Button>
                          </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>

    )
}

