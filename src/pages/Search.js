import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

export default function Search(product) {

  const {name, description, price, _id , isActive} = product;

return (
  <Card>
      <Card.Body>
          <Card.Title>Product Search</Card.Title>
          
          <Button className="bg-primary" as={Link} to={`/ProductsAll`}>Show All Active Products</Button>
          <Button className="bg-primary" as={Link} to={`/searchproduct`} >Find Product</Button>
      </Card.Body>
  </Card>
)
}

Search.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    isActive: PropTypes.bool
  })
}