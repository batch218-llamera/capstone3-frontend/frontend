import Banner from '../components/Banner';

export default function Home() {

const data = {
	title: "Explore the world, from the comfort of your home",
	content: "Enjoy the highest-immersion virtual and mixed reality products and services for advanced VR users.",
	destination:"/productsall",
	label: "Start Exploring"
}

	return (
		<>
		    <Banner data={data} />
		</>
		)
}





