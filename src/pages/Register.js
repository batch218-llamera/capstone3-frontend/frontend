import { useState, useEffect,useContext } from 'react';
import { useNavigate, Link, Navigate } from 'react-router-dom'; 

import Swal from 'sweetalert2'; 

import UserContext from '../UserContext'; 
import { Form, Button, Row, Col, Container } from 'react-bootstrap';

export default function Register() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate(); 

    const [firstName, setFirstName] = useState(""); 
    const [lastName, setLastName] = useState(""); 
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);

    function registerUser(e) {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {

                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Kindly provide another email to complete registration."
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true) {
                    // Clear input fields
                    setFirstName("");
                    setLastName("")
                    setEmail("");
                    setMobileNo("");
                    setPassword1("");
                    setPassword2("");

                    Swal.fire({
                            title: "Registration Successful!",
                            icon: "success",
                            text: "Let's Start Exploring!"
                        })

                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }
                })
            }
        })

    }

    useEffect(() => {
            if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [email, password1, password2])

    return (
        (user.id !== null) ? 
        <Navigate to ="/home" /> 
        : 
        <Form style={{ width: "100%" }} onSubmit={(e) => registerUser(e)}>
            <Container>
            <Row>
            <Col md={6} className="mt-5">
            <h1 className="mb-4 mt-4">Create an account</h1>
          <Form.Group className="mb-3" controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control 
                type="text"
                style={{ width: "60%" }}
                value={lastName}
                onChange={(e) => {setLastName(e.target.value)}}
                placeholder="Enter your Last Name" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email"
                style={{ width: "60%" }}
                value={email}
                onChange={(e) => {setEmail(e.target.value)}}
                placeholder="Enter email" />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="mobileNo">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control 
                type="text"
                style={{ width: "60%" }}
                value={mobileNo}
                onChange={(e) => {setMobileNo(e.target.value)}}
                placeholder="0999999999" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                style={{ width: "60%" }}
                value={password1}
                onChange={(e) => {setPassword1(e.target.value)}}
                placeholder="Enter Your Password" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control 
                type="password"
                style={{ width: "60%" }} 
                value={password2}
                onChange={(e) => {setPassword2(e.target.value)}}
                placeholder="Verify Your Password" />
          </Form.Group>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                     Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                      Submit
                    </Button>
                }
                <p className="pt-4">
                    Already have an account? <Link to="/login">Login</Link>{" "}
                </p>
                </Col>
                <Col md={6} className="mt-5 signup__image--container"></Col>
            </Row>
            </Container>
        </Form> 
    )

}

