import {useState, useEffect, useContext} from 'react';
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import {Link, useParams} from 'react-router-dom';

export default function ProductCard({product}) {

  const {name, description, price, _id , isActive} = product;
  const { user } = useContext(UserContext);
  const { productId } = useParams();
   const { userId } = useParams();


  return (
  <Card>
      <Card.Body>
          <Card.Title>{product.title}</Card.Title>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>
           {
                    (user.id !== null) ?
                      <>
                      
                      </>
                      :
                      <Button className="btn btn-danger" as={Link} to="/login"  >Login to Add to Cart</Button>
                  }

          <Button className="bg-primary" as={Link} to={`/productview/${_id}`} >Product Details</Button>
          
      </Card.Body>
  </Card>
  )
}


ProductCard.propTypes = {
  
  product: PropTypes.shape({
    
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    isActive: PropTypes.bool


  })
}
