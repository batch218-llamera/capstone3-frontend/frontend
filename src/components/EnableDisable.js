import { useState, useEffect,useContext } from 'react'; 

import {Navigate, useParams} from 'react-router-dom'; 
import {useNavigate} from 'react-router-dom'; 

import Swal from 'sweetalert2'; 

import UserContext from '../UserContext'; 

import { Form, Button } from 'react-bootstrap';



export default function EnableDisable() {

    
    const { productId } = useParams();
    const {user} = useContext(UserContext); 

    const navigate = useNavigate(); 

   
    const [isActive, setIsActive] = useState("");
    const [isActiveButton, setIsActiveButton] = useState("false");
 

    
    function changeStatus(productId, e) {
        
        e.preventDefault()

        
         fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {


                    method: "PUT",
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`

                    },
                    body: JSON.stringify({
                        productId: productId,
                        isActive: isActive,
                     
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    console.log(productId)

                    if((isActive == true)) {
            
              
                    Swal.fire({
                            title: "Product is now available",
                            icon: "success",
                            text: ""
                        })

                        navigate("/products");

                    } else {

                        Swal.fire({
                            title: "Product is unavailable",
                            icon: "success",
                            text: ""
                        })

                        navigate("/products");
                    }
                })

       
    }

    
    useEffect(() => {
    
        

            
            if((isActiveButton !== '')){
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [isActiveButton])


    return (
     
        <Form onSubmit={(e) => changeStatus(productId, e)}>

           
            <Form.Group className="mb-3" controlId="isActive">
            <Form.Label>Product Availability ["true or false"]</Form.Label>
            <Form.Control 
                type="text"
                value={isActive}
                onChange={(e) => {setIsActive(e.target.value)}}
                placeholder="True or False" 
                required
                />
          </Form.Group>

     
          { isActiveButton ?
                    <Button variant="primary" type="submit" id="submitBtn">
                     Change Status
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                      Update
                    </Button>
          }
         
        </Form> 
    )

}

