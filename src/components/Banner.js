import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner({data}) {
    const{title, content, destination, label} = data;
return (
    <div className="main-banner"  style={{ width: "100%" }}>
    <Row>
        <Col md={8} xs={12} className="p-5 text-white">
                <h1>{title}</h1>
                <p>{content}</p>
                <Button className="button" variant="primary" as={Link} to={destination}>{label}</Button>
        </Col>
    </Row>
    </div>
    )
}

