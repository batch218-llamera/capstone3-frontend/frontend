import { useState, useEffect,useContext } from 'react'; 

import {Navigate, useParams} from 'react-router-dom'; 
import {useNavigate} from 'react-router-dom'; 

import Swal from 'sweetalert2'; 

import UserContext from '../UserContext'; 

import { Form, Button } from 'react-bootstrap';



export default function Update() {

    
    const { productId } = useParams();
    const {user} = useContext(UserContext); 

    const navigate = useNavigate(); 

   
    const [name, setName] = useState(""); 
    const [description, setDescription] = useState(""); 
    const [price, setPrice] = useState("");
    const [isActive, setIsActive] = useState(false);

 

    
    function UpdateProduct(productId, e) {
        
        e.preventDefault()

        
         fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {


                    method: "PUT",
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`

                    },
                    body: JSON.stringify({
                        productId: productId,
                        name: name,
                        description: description,
                        price: price,
                     
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    console.log(productId)

                    if((name !== '' && description !== '' && price !== '')) {
            
                /*    setName("");
                    setDescription("")
                    setPrice("");*/
             

                    Swal.fire({
                            title: "Product updated successfully!",
                            icon: "success",
                            text: ""
                        })

                        navigate("/products");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }
                })

       
    }

    
    useEffect(() => {
    
        

            
            if((name !== '' && description !== '' && price !== '')){
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [name, description, price])


    return (
     /*   (user.id !== null) ? 
        <Navigate to ="/" /> 
        : // S54 ACTIVITY*/
        <Form onSubmit={(e) => UpdateProduct(productId, e)}>

           
            <Form.Group className="mb-3" controlId="name">
            <Form.Label>Product Name</Form.Label>
            <Form.Control 
                type="text"
                value={name}
                onChange={(e) => {setName(e.target.value)}}
                placeholder="Enter Product Name" 
                required
                />
          </Form.Group>

          <Form.Group className="mb-3" controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control 
                type="text"
                value={description}
                onChange={(e) => {setDescription(e.target.value)}}
                placeholder="Enter Description" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="price">
            <Form.Label>Price</Form.Label>
            <Form.Control 
                type="number"
                value={price}
                onChange={(e) => {setPrice(e.target.value)}}
                placeholder="Enter Price" />
            <Form.Text className="text-muted">
              
            </Form.Text>
          </Form.Group>

        
     
          { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                     Add
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                      Update
                    </Button>
          }
         
        </Form> 
    )

}

