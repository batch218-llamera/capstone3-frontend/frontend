import {UserProvider} from './UserContext';
import AppNavbar from './components/AppNavBar';
import ProductView from './components/ProductView';
import Update from './components/ProductUpdate';
import ShowAllProducts from './components/ShowAllProducts';
import EnableDisable from './components/EnableDisable';
import AllProducts from './pages/AllProducts';
import SearchProduct from './pages/SingleProduct';
import Orders from './pages/Orders';

import Home from './pages/Home';
import Error from './pages/Error';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Add from './pages/CreateProduct';
import Products from './pages/Products';
import Search from './pages/Search';

import {useState, useEffect} from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import {Container} from 'react-bootstrap';
import './App.css';

function App() {
const[user, setUser] = useState({
  id: null,
  isAdmin: null
})

const unsetUser = () => {
  localStorage.clear();
}

useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
      //user is logged in
        if(typeof data._id !== "undefined") {

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        } 
        else { 
            setUser({
                id: null,
                isAdmin: null
            })
        }

    })
}, []);


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <AppNavbar />
        <Container>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/addproduct" element={<Add/>} />
                <Route path="/products" element={<Products/>} />
                <Route path="/update/:productId" element={<Update/>} />
                <Route path="/enabledisable/:productId" element={<EnableDisable/>} />
                <Route path="/search" element={<Search/>} />
                <Route path="/showallactiveproducts" element={<ShowAllProducts/>} />
                <Route path="/ProductsAll" element={<AllProducts/>} />
                <Route path="/searchproduct" element={<SearchProduct/>} />
                <Route path="/productview/:productId" element={<ProductView/>} />
                <Route path="/user/:userId" element={<Orders/>} />
                <Route path="/*" element={<Error />} />
            </Routes>
        </Container>
    </Router>
    </UserProvider>
   </>
  );
}

export default App;
